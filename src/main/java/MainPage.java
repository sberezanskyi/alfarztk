import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MainPage {

    private static final String MAIN_PAGE_URL ="https://allo.ua/ua/skladnaja-mebel/shezlong-kingcamp-arms-chairin-steel-kc3818-blue.html";
    private WebDriver driver;

    private By addToCart = By.xpath("//button[contains(@class,'p-buy-button buy-button buy-button--primary buy-button--default buy-button--cart p-trade__buy-button')]"); //добавили в корзину

    @FindBy(xpath= "//*[@class=\"p-view__header-title\"]")  //название товара на странице товара
    private WebElement fieldNameOfGoodsA;

    @FindBy(xpath= "//*[@class='text']/span")  //название товара в корзине
    private WebElement fieldNameOfGoodsB;

    @FindBy(xpath= "/html[1]/body[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[2]/main[1]/div[5]/div[1]/div[1]/div[1]/div[2]/span[1]")  //цена товара на странице товара
    private WebElement fieldPriceOfGoodsA;

    @FindBy(xpath= "//*[@class='price-box__cur']")  //цена товара в корзине
    private WebElement fieldPriceOfGoodsB;


    public MainPage(WebDriver driver) {
        this.driver = driver;
    }

    public MainPage openMainPage(){
        this.driver.navigate().to(MAIN_PAGE_URL);  //открыли главную страницу
        return this;
    }

    public MainPage addToCart() throws InterruptedException {
        this.driver.findElement(addToCart).click(); //добавили в корзину
        Thread.sleep(7000);
        return this;
    }

    public String checkNameOfGoodsA(){            //взяли наименование со страницы товара
        return fieldNameOfGoodsA.getText();
    }

    public String checkNameOfGoodsB(){            //взяли наименование с корзины
        return fieldNameOfGoodsB.getText();
    }

    public String checkPriceOfGoodsA(){                                                 //взяли цену со страницы товара
        return fieldPriceOfGoodsA.getText().trim().replace("₴", "");
    }

    public String checkPriceOfGoodsB(){                                                 //взяли цену с корзины
        return fieldPriceOfGoodsB.getText().trim().replace("₴", "");
    }

}