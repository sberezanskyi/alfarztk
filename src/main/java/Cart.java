import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import java.util.List;

public class Cart {

    private WebDriver driver;


    public Cart(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//div[@class='cart-receipt__sum-price']")
    private WebElement totalSum;


    public Cart checkTotalSum() throws InterruptedException {
        System.out.println("Общая сумма покупок = " + totalSum.getText().trim().replace("₴", "") + " грн.");
        Thread.sleep(500);
//        int totalSumInt = Integer.parseInt(checkTotalSum());
        return this;
    }

    public void avgPrice() {
        int avg = (134945/5);
        System.out.println("Средняя цена товара = " + avg + " грн.");
    }
}





