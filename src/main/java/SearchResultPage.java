import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import java.util.List;

public class SearchResultPage<isAvailablePhone> {

    private WebDriver driver;

    @FindBy(xpath = "//span[contains(.,'Мобильные телефоны')]")
    private WebElement mobilePhone;

    @FindBy(xpath = "//span[contains(.,'Защитные пленки')]")
    private WebElement screenProtect;

    @FindBy(xpath = "//label[@for='Rozetka']")
    private WebElement checkboxRozetka;

    @FindBy(xpath = "//*[@class='goods-tile__price-value']")
    private WebElement price;

    @FindBy(xpath = "//button[@class=\"buy-button goods-tile__buy-button ng-star-inserted\"]")
    private WebElement addToCart;

    @FindBy(xpath = "//rz-cart[@class='header-actions__component']")
    private WebElement openCart;


    public SearchResultPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    public SearchResultPage clickMobilePhone() throws InterruptedException {
        mobilePhone.click();
        Thread.sleep(500);
        return this;
    }

    public SearchResultPage clickcheckboxRozetka() throws InterruptedException {
        checkboxRozetka.click();
        Thread.sleep(500);
        return this;
    }

    public int isAvailablePhone() {
        List<WebElement> isAvailablePhone = driver.findElements(By.xpath("//div[@class=\"goods-tile__availability goods-tile__availability--available ng-star-inserted\"]"));
        int countPhone = isAvailablePhone.size();
        System.out.println("В Розетке в наличии "+countPhone+" шт. iPhone 12 Pro Max");
        return countPhone;
    }

    public void clickButtonCart() throws InterruptedException {
        List<WebElement> addToCart = driver.findElements(By.xpath("//button[@class=\"buy-button goods-tile__buy-button ng-star-inserted\"]"));
        for (int i = 0; i < addToCart.size(); i++) {
            addToCart.get(i).click();
            Thread.sleep(50);
        }

    }

    public SearchResultPage clickScreenProtect() throws InterruptedException {
        screenProtect.click();
        Thread.sleep(500);
        return this;

    }

    public int isAvailableScreenProtect() {
        List<WebElement> isAvailableScreenProtect = driver.findElements(By.xpath("//div[@class=\"goods-tile__availability goods-tile__availability--available ng-star-inserted\"]"));
        int countScreenProtect = isAvailableScreenProtect.size();
        System.out.println("В Розетке в наличии "+countScreenProtect+" шт. пленок на iPhone 12 Pro Max");

        return countScreenProtect;
    }

    public Cart clickOpenCart() {
        openCart.click();
        return new Cart(driver);

    }


}
