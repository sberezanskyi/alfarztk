import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class MainPageTest {
    private WebDriver driver;
    private MainPage mainPage;
    private MainPageForIPhone mainPageForIPhone;
    private SearchResultPage searchPage;
    private Cart cart;

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
//      System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver.exe");

        driver = new ChromeDriver();
//      driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(200, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);

        mainPage = new MainPage(this.driver);
        mainPageForIPhone = new MainPageForIPhone(this.driver);

    }

    @Test   //задание 1
    public void findGoods() throws InterruptedException {
        mainPage.openMainPage();
        mainPage.addToCart();
        Assert.assertEquals(mainPage.checkNameOfGoodsA(), mainPage.checkNameOfGoodsB());
        Assert.assertEquals(mainPage.checkPriceOfGoodsA(), mainPage.checkPriceOfGoodsB());

    }

    @Test   //задание 2
    public void findIPhone() throws InterruptedException {
        mainPageForIPhone.openMainPageForIPhone();
        searchPage = mainPageForIPhone.editSearchField("iphone 12 pro max").clickButtonSearch();
        searchPage.clickMobilePhone().clickcheckboxRozetka();
        searchPage.isAvailablePhone();
        searchPage.clickButtonCart();
        searchPage.clickScreenProtect().clickcheckboxRozetka();
        searchPage.isAvailableScreenProtect();
        searchPage.clickButtonCart();
        cart = searchPage.clickOpenCart();
        cart.checkTotalSum().avgPrice();

    }

    @After
    public void cleanup(){
        driver.manage().deleteAllCookies();
        driver.close();
    }
}